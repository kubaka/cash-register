package com.jk.cashregister.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
public class MainController {
		@GetMapping(value = {"/","/index","/home"})
		public String getIndexPage() {
				return "/index";
		}

		@GetMapping("/login")
		public String getLoginPage() {
				return "/login";
		}

		@GetMapping("/logout-success")
		public String getLogoutPage() {
				return "/logout";
		}
}
